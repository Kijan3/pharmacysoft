# PharmacySoft

> A simple program to handle sales at the pharmacy. IN PROGRESS
## Table of contents
* [General info](#general-info)
* [Screenshots](#screenshots)
* [Technologies](#technologies)
* [Features](#features)
* [Status](#status)
* [Contact](#contact)

## General info

Connection to mySQL database containing people who can operate the program 
as well as their authorization to dispense specific medicinal products.

Checks the correctness of prescription data such as: prescription number, 
doctor's number, medical center number, PESEL - patient's id, prescription 
date for medicines (30 days) and antibiotics (7 days).


## Screenshots

## Technologies

## Features

## Status
Project is: _in progress_

## Contact
Created by MKijanski.
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.MatteBorder;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.CaretEvent;

public class LoginPage extends JFrame {

	private JPanel contentPane;
	private final JPanel panel = new JPanel();
	private JTextField txtFieldLogin;
	private JPasswordField passwordField;
	private String fontName = "Gentium Basic";
	private Color myBlue = new Color(67, 94, 137);
	private Color myGreen = new Color(60, 179, 113);
	private Color myRed = new Color(255, 51, 51);
	private JLabel lblExit;
	private JLabel lblNewAccount;
	private JLabel lblWarningLogin;
	private JLabel lblWarningPass;

	LoginUtilities lu = new LoginUtilities();

	/**
	 * Methods.
	 * 
	 * @throws Exception
	 */

	private void logMeIn() throws Exception {
		Connection con = myConnection.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			ps = con.prepareStatement(
					"SELECT `id`, `name`, `lastname`, `degre`, `login`, `password` FROM `user` WHERE `login` = ? AND `password` = ?");
			ps.setString(1, txtFieldLogin.getText());
			ps.setString(2, String.valueOf(passwordField.getPassword()));
			rs = ps.executeQuery();

			if (rs.next()) {
				// Przejscie do okna g��wnego =======================================
				System.out.println("Uda�o si� zalogowa�" + ", " + rs.getString(4));
//				this.dispose();
//				JOptionPane.showMessageDialog(null, "Witaj " + rs.getString(2));

			} else {
				System.out.println("B��d");
			}

		} catch (SQLException e) {
			System.out.println("B��d " + e.getMessage());
		} finally {
			try {
				rs.close();
			} catch (Exception e) {
			}
			try {
				ps.close();
			} catch (Exception e) {
			}
			try {
				con.close();
			} catch (Exception e) {
			}
		}

	}

	private void CreateNewAccount() {
		CreateNewAccount cna = new CreateNewAccount();
		cna.setVisible(true);
		this.dispose();
	}

	private void exitFrame() {
		System.exit(3);
	}

	/**
	 * Launch the application.
	 */

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginPage frame = new LoginPage();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LoginPage() {
		setUndecorated(true);
		setTitle("Login");
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int width = dim.width / 2 - 450;
		int height = dim.height / 2 - 300;
		setIconImage(Toolkit.getDefaultToolkit()
				.getImage("C:\\Users\\Kijan\\Desktop\\JAVA\\PharmacySoft\\src\\Images\\pharmacy-small.png"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(width, height, 900, 600);
		contentPane = new JPanel();
		contentPane.setBackground(myBlue);
		contentPane.setBorder(null);
		setContentPane(contentPane);
		contentPane.setLayout(null);
		panel.setBorder(new MatteBorder(0, 0, 0, 2, (Color) new Color(255, 255, 255)));
		panel.setBackground(myGreen);
		panel.setBounds(0, 0, 440, 600);
		contentPane.add(panel);
		panel.setLayout(null);

		JLabel lblLogo = new JLabel("");
		lblLogo.setIcon(new ImageIcon("C:\\Users\\Kijan\\Desktop\\JAVA\\PharmacySoft\\src\\Images\\pharmacy-big.png"));
		lblLogo.setBounds(92, 160, 256, 256);
		panel.add(lblLogo);

		JLabel lblNewLabel = new JLabel("PharmacySoft  ");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setForeground(new Color(255, 255, 255));
		lblNewLabel.setFont(new Font(fontName, Font.BOLD, 44));
		lblNewLabel.setBounds(0, 64, 440, 64);
		panel.add(lblNewLabel);

		JLabel lblPharmacySoft = new JLabel("PharmacySoft  ");
		lblPharmacySoft.setHorizontalAlignment(SwingConstants.CENTER);
		lblPharmacySoft.setForeground(Color.WHITE);
		lblPharmacySoft.setFont(new Font(fontName, Font.BOLD, 24));
		lblPharmacySoft.setBounds(0, 446, 440, 64);
		panel.add(lblPharmacySoft);

		JLabel lblLogin = new JLabel("Login");
		lblLogin.setForeground(Color.WHITE);
		lblLogin.setFont(new Font(fontName, Font.BOLD, 24));
		lblLogin.setBounds(497, 89, 320, 40);
		contentPane.add(lblLogin);

		txtFieldLogin = new JTextField();
		txtFieldLogin.addCaretListener(new CaretListener() {
			public void caretUpdate(CaretEvent arg0) {
				lu.userDataValidator(txtFieldLogin.getText(), lblWarningLogin);
			}
		});
		txtFieldLogin.setToolTipText("Login - minimum 3 znaki");
		txtFieldLogin.setCaretColor(new Color(255, 255, 255));
		txtFieldLogin.setForeground(new Color(255, 255, 255));
		txtFieldLogin.setFont(new Font(fontName, Font.BOLD, 24));
		txtFieldLogin.setBorder(new MatteBorder(0, 0, 2, 0, (Color) new Color(255, 255, 255)));
		txtFieldLogin.setOpaque(false);
		txtFieldLogin.setBounds(497, 152, 320, 40);
		contentPane.add(txtFieldLogin);
		txtFieldLogin.setColumns(10);

		JLabel lblHaso = new JLabel("Has\u0142o");
		lblHaso.setForeground(Color.WHITE);
		lblHaso.setFont(new Font(fontName, Font.BOLD, 24));
		lblHaso.setBounds(497, 250, 320, 40);
		contentPane.add(lblHaso);

		passwordField = new JPasswordField();
		passwordField.addCaretListener(new CaretListener() {
			public void caretUpdate(CaretEvent e) {

				lu.userPasswordValidator(passwordField.getPassword(), lblWarningPass);
			}
		});
		passwordField.setToolTipText(
				"Has\u0142o - minimum 4 znaki, musi zawiera\u0107 conajmniej jedn\u0105 cyfr\u0119, jedn\u0105 wielk\u0105 liter\u0119 i znak specjalny");
		passwordField.setCaretColor(new Color(255, 255, 255));
		passwordField.setBorder(new MatteBorder(0, 0, 2, 0, (Color) new Color(255, 255, 255)));
		passwordField.setOpaque(false);
		passwordField.setForeground(new Color(255, 255, 255));
		passwordField.setFont(new Font(fontName, Font.BOLD, 28));
		passwordField.setBounds(497, 313, 320, 40);
		contentPane.add(passwordField);

		JButton btnNewButton = new JButton("Zaloguj");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnNewButton.setFont(new Font(fontName, Font.BOLD, 28));
			}

			@Override
			public void mouseExited(MouseEvent e) {
				btnNewButton.setFont(new Font(fontName, Font.BOLD, 24));
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					logMeIn();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnNewButton.setBorder(new LineBorder(new Color(255, 255, 255), 2));
		btnNewButton.setBackground(myBlue);
		btnNewButton.setOpaque(false);
		btnNewButton.setForeground(new Color(255, 255, 255));
		btnNewButton.setFont(new Font(fontName, Font.BOLD, 24));
		btnNewButton.setBounds(575, 411, 181, 47);
		contentPane.add(btnNewButton);

		lblExit = new JLabel("X");
		lblExit.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				exitFrame();
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				lblExit.setFont(new Font(fontName, Font.BOLD, 44));
			}

			@Override
			public void mouseExited(MouseEvent e) {
				lblExit.setFont(new Font(fontName, Font.BOLD, 40));
			}
		});
		lblExit.setForeground(new Color(255, 255, 255));
		lblExit.setFont(new Font(fontName, Font.BOLD, 40));
		lblExit.setBounds(858, 11, 32, 40);
		contentPane.add(lblExit);

		lblNewAccount = new JLabel("Za\u0142\u00F3\u017C nowe konto");
		lblNewAccount.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				lblNewAccount.setFont(new Font(fontName, Font.BOLD, 20));
			}

			@Override
			public void mouseExited(MouseEvent e) {
				lblNewAccount.setFont(new Font(fontName, Font.BOLD, 18));
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				CreateNewAccount();
			}
		});
		lblNewAccount.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewAccount.setForeground(Color.WHITE);
		lblNewAccount.setFont(new Font(fontName, Font.BOLD, 18));
		lblNewAccount.setBounds(441, 507, 459, 40);
		contentPane.add(lblNewAccount);

		lblWarningLogin = new JLabel("*");
		lblWarningLogin.setVisible(false);
		lblWarningLogin.setHorizontalAlignment(SwingConstants.CENTER);
		lblWarningLogin.setForeground(myRed);
		lblWarningLogin.setFont(new Font(fontName, Font.BOLD, 24));
		lblWarningLogin.setBounds(827, 164, 32, 28);
		contentPane.add(lblWarningLogin);

		lblWarningPass = new JLabel("*");
		lblWarningPass.setVisible(false);
		lblWarningPass.setHorizontalAlignment(SwingConstants.CENTER);
		lblWarningPass.setForeground(new Color(255, 51, 51));
		lblWarningPass.setFont(new Font(fontName, Font.BOLD, 24));
		lblWarningPass.setBounds(827, 313, 32, 28);
		contentPane.add(lblWarningPass);
	}
}

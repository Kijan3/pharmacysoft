import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.swing.JOptionPane;

public class myConnection {
	public static Connection getConnection() throws Exception {
		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost/pharmacysoft", "root", "");
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Brak po��czenia z baz� danych" + e.getMessage());
		}
		return con;
	}
}

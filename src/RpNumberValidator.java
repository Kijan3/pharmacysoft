
public class RpNumberValidator {
	// 0216010000005609914486 = 22 cyfry
	public boolean rpNumberIsValid;
	private byte[] rpNumberArr = new byte[22];
	public int nfzNumber;

	public RpNumberValidator(String rpNumber) {

		if (rpNumber.length() != 22) {
			rpNumberIsValid = false;
		} else {
			for (int i = 0; i < rpNumber.length(); i++) {
				try {
					rpNumberArr[i] = Byte.parseByte(rpNumber.substring(i, i + 1));
				} catch (NumberFormatException e) {
					rpNumberIsValid = false;
					break;
				}
			}
			if (checkNumber() && prescriptionDigits()) {
				rpNumberIsValid = true;
			} else {
				rpNumberIsValid = false;
			}
		}
	}

	public boolean checkNumber() {
		int sum = 7 * rpNumberArr[0] + 9 * rpNumberArr[1] + 1 * rpNumberArr[2] + 3 * rpNumberArr[3] + 7 * rpNumberArr[4]
				+ 9 * rpNumberArr[5] + 1 * rpNumberArr[6] + 3 * rpNumberArr[7] + 7 * rpNumberArr[8] + 9 * rpNumberArr[9]
				+ 1 * rpNumberArr[10] + 3 * rpNumberArr[11] + 7 * rpNumberArr[12] + 9 * rpNumberArr[13]
				+ 1 * rpNumberArr[14] + 3 * rpNumberArr[15] + 7 * rpNumberArr[16] + 9 * rpNumberArr[17]
				+ 1 * rpNumberArr[18] + 3 * rpNumberArr[19] + 7 * rpNumberArr[20];

		sum %= 10;

		if (sum == rpNumberArr[21] && sumDigits(rpNumberArr)) {
			return true;
		} else {
			return false;
		}
	}

	private boolean prescriptionDigits() {
		if (rpNumberArr[0] == 0 && rpNumberArr[1] == 2) {
			return true;
		} else {
			return false;
		}
	}

	private boolean sumDigits(byte[] arr) {
		int z = 0;
		for (byte x : arr) {
			z += x;
		}
		if (z == 0) {
			return false;
		} else
			return true;
	}

	public String OddzialWojewodzki() {
		String nfzString = String.valueOf(rpNumberArr[2]) + String.valueOf(rpNumberArr[3]);
		String nfzOddzial;

		switch (nfzString) {
		case "01":
			nfzOddzial = "Dolno�l�ski";
			nfzNumber = 1;
			break;
		case "02":
			nfzOddzial = "Kujawsko-Pomorski";
			nfzNumber = 2;
			break;
		case "03":
			nfzOddzial = "Lubelski";
			nfzNumber = 3;
			break;
		case "04":
			nfzOddzial = "Lubuski";
			nfzNumber = 4;
			break;
		case "05":
			nfzOddzial = "��dzki";
			nfzNumber = 5;
			break;
		case "06":
			nfzOddzial = "Ma�opolski";
			nfzNumber = 6;
			break;
		case "07":
			nfzOddzial = "Mazowiecki";
			nfzNumber = 7;
			break;
		case "08":
			nfzOddzial = "Opolski";
			nfzNumber = 8;
			break;
		case "09":
			nfzOddzial = "Podkarpacki";
			nfzNumber = 9;
			break;
		case "10":
			nfzOddzial = "Podlaski";
			nfzNumber = 10;
			break;
		case "11":
			nfzOddzial = "Pomorski";
			nfzNumber = 11;
			break;
		case "12":
			nfzOddzial = "�l�ski";
			nfzNumber = 12;
			break;
		case "13":
			nfzOddzial = "�wi�tokrzyski";
			nfzNumber = 13;
			break;
		case "14":
			nfzOddzial = "Warmi�sko-Mazurski";
			nfzNumber = 14;
			break;
		case "15":
			nfzOddzial = "Wielkopolski";
			nfzNumber = 15;
			break;
		case "16":
			nfzOddzial = "Zachodniopomorski";
			nfzNumber = 16;
			break;
		default:
			nfzOddzial = "Nierozpoznany oddzia� NFZ";
			break;
		}
		return nfzOddzial;
	}
}

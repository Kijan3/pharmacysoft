public class RegonValidator {
	private byte[] regonShortArr = new byte[9];
	private byte[] regonLongArr = new byte[14];
	public boolean regonIsValid = false;

	public RegonValidator(String regon) {
		if (regon.length() != 9 && regon.length() != 14) {
			regonIsValid = false;
		} else {
			if (regon.length() == 9) {
				for (int i = 0; i < regon.length(); i++) {
					try {
						regonShortArr[i] = Byte.parseByte(regon.substring(i, i + 1));
					} catch (NumberFormatException e) {
						regonIsValid = false;
						break;
					}
				}
				if (checkShortSum() && sumDigits(regonShortArr) != 0) {
					regonIsValid = true;
				} else
					regonIsValid = false;
			} else if (regon.length() == 14) {
				for (int i = 0; i < regon.length(); i++) {
					try {
						regonLongArr[i] = Byte.parseByte(regon.substring(i, i + 1));
					} catch (NumberFormatException e) {
						regonIsValid = false;
						break;
					}
				}
				if (checkLongSum() && sumDigits(regonLongArr) != 0) {
					regonIsValid = true;
				} else
					regonIsValid = false;
			} else {
				regonIsValid = false;
			}
		}
	}

	private boolean checkShortSum() {
		int sum = 8 * regonShortArr[0] + 9 * regonShortArr[1] + 2 * regonShortArr[2] + 3 * regonShortArr[3]
				+ 4 * regonShortArr[4] + 5 * regonShortArr[5] + 6 * regonShortArr[6] + 7 * regonShortArr[7];
		sum %= 11;
		if (sum == 10) {
			sum = 0;
		}
		if (sum == regonShortArr[8]) {
			return true;
		} else {
			return false;
		}
	}

	private boolean checkLongSum() {
		int sum = 2 * regonLongArr[0] + 4 * regonLongArr[1] + 8 * regonLongArr[2] + 5 * regonLongArr[3]
				+ 0 * regonLongArr[4] + 9 * regonLongArr[5] + 7 * regonLongArr[6] + 3 * regonLongArr[7]
				+ 6 * regonLongArr[8] + 1 * regonLongArr[9] + 2 * regonLongArr[10] + 4 * regonLongArr[11]
				+ 8 * regonLongArr[12];
		sum %= 11;
		if (sum == 10) {
			sum = 0;
		}
		if (sum == regonLongArr[13]) {
			return true;
		} else {
			return false;
		}
	}

	private int sumDigits(byte[] arr) {
		int z = 0;
		for (byte x : arr) {
			z += x;
		}
		return z;
	}
}

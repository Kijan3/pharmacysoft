import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JLabel;

public class LoginUtilities {

	public void userDataValidator(String userData, JLabel warningLabel) {
		if (userData.equals(null) || userData.length() < 3) {
			warningLabel.setVisible(true);
		} else {
			warningLabel.setVisible(false);
		}
	}

	public boolean userPasswordValidator(char[] pass, JLabel warningLabel) {
		String password = String.valueOf(pass);
		Pattern pattern = Pattern.compile("^(?=.*[A-Z])(?=.*[!@#$%^&*]).*[a-z].*[0-9]");
		Matcher matcher = pattern.matcher(password);
		boolean b = matcher.find();

		if (password.length() < 4 || !b) {
			warningLabel.setVisible(true);
			return false;
		} else {
			warningLabel.setVisible(false);
			return true;
		}
	}

}

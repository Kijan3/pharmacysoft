import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.border.MatteBorder;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import javax.swing.border.LineBorder;
import javax.swing.SwingConstants;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.event.CaretListener;
import javax.swing.event.CaretEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class CreateNewAccount extends JFrame {

	private JPanel contentPane;
	private JPanel panel_1;

	private String fontName = "Gentium Basic";
	private Color myBlue = new Color(67, 94, 137);
	private Color myGreen = new Color(60, 179, 113);
	private Color myRed = new Color(255, 51, 51);

	private JTextField textFieldName;
	private JTextField textFieldLastName;
	private JComboBox comboBoxGrade;
	private JTextField textFieldLogin;
	private JPasswordField passwordField;

	private JButton bttnCreateAccount;
	private JButton bttnClear;

	private JLabel lblLogin;
	private JLabel lblPassword;
	private JLabel lblGoToLogin;
	private JLabel lblWarningName;
	private JLabel lblWarningLName;
	private JLabel lblWarningGrade;
	private JLabel lblWarningLogin;
	private JLabel lblWarningPassword;

	LoginUtilities lu = new LoginUtilities();

	/**
	 * Metody =============================
	 */

	private boolean userDataValidator() {
		if (textFieldName.getText().length() < 3) {
			JOptionPane.showMessageDialog(null, "Imie musi zawiera� co najmniej 3 znaki");
			return false;
		} else if (textFieldLastName.getText().length() < 3) {
			JOptionPane.showMessageDialog(null, "Nazwisko musi zawiera� co najmniej 3 znaki");
			return false;
		} else if (comboBoxGrade.getSelectedItem().toString().equals("")) {
			JOptionPane.showMessageDialog(null, "Wybierz tytu�");
			return false;
		} else if (textFieldLogin.getText().length() < 4) {
			JOptionPane.showMessageDialog(null, "Login musi zawiera� co najmniej 4 znaki");
			return false;
		} else {
			return true;
		}
	}

	private boolean isUserExist() throws Exception {
		boolean uExist = false;
		Connection con = myConnection.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = con.prepareStatement("SELECT * FROM `user` WHERE `login` = ?");
			ps.setString(1, textFieldLogin.getText());
			rs = ps.executeQuery();
			if (rs.next()) {
				uExist = false;
				JOptionPane.showMessageDialog(null, "U�ytkownik o tym loginie istnieje w bazie, wybierz inny login");
			} else {
				uExist = true;
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "B��d po��czenia z baz� danych " + e.getMessage());
		} finally {
			try {
				rs.close();
			} catch (Exception e) {
			}
			try {
				ps.close();
			} catch (Exception e) {
			}
			try {
				con.close();
			} catch (Exception e) {
			}
		}
		return uExist;
	}

	private void createNewUserAccount() throws Exception {
//		Connection con = myConnection.getConnection();
//		PreparedStatement ps = null;

		if (userDataValidator() && (lu.userPasswordValidator(passwordField.getPassword(), lblWarningPassword))) {
			isUserExist();
		}

//		try
//		{
//			ps = con.prepareStatement("INSERT INTO `user`(`name`, `lastname`, `degre`, `login`, `password`) VALUES (?,?,?,?,?)");
//			ps.setString(1, textFieldName.getText());
//			ps.setString(2, textFieldLastName.getText());
//			ps.setString(3, comboBoxGrade.getSelectedItem().toString());
//			ps.setString(4, textFieldLogin.getText());
//			ps.setString(5, String.valueOf(passwordField.getPassword()));
//			
//			//If User !exists================ executeUpdate======================>
//			
//			ps.executeUpdate();
//			
//		} catch (SQLException e)
//		{
//			JOptionPane.showMessageDialog(null, "B��d " + e.getMessage());
//		}
//		finally {
//		    try { ps.close(); } catch (Exception e) { }
//		    try { con.close(); } catch (Exception e) { }
//		}
	}

	private void clearData() {
		textFieldName.setText(null);
		textFieldLastName.setText(null);
		comboBoxGrade.setSelectedIndex(0);
		textFieldLogin.setText(null);
		passwordField.setText(null);
	}

	private void goToLogin() {
		LoginPage lp = new LoginPage();
		lp.setVisible(true);
		this.dispose();
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CreateNewAccount frame = new CreateNewAccount();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CreateNewAccount() {
		setTitle("Nowe Konto");
		setIconImage(
				Toolkit.getDefaultToolkit().getImage(CreateNewAccount.class.getResource("/Images/pharmacy-small.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		int width = dimension.width / 2 - 250;
		int height = dimension.height / 2 - 400;
		setBounds(width, height, 500, 800);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBackground(myBlue);
		panel.setBounds(0, 0, 486, 761);
		contentPane.add(panel);
		panel.setLayout(null);

		panel_1 = new JPanel();
		panel_1.setLayout(null);
		panel_1.setBackground(myBlue);
		panel_1.setBounds(0, 0, 486, 761);
		panel.add(panel_1);

		lblLogin = new JLabel("Login");
		lblLogin.setForeground(Color.WHITE);
		lblLogin.setFont(new Font(fontName, Font.BOLD, 24));
		lblLogin.setBounds(84, 383, 320, 40);
		panel_1.add(lblLogin);

		textFieldLogin = new JTextField();
		textFieldLogin.setToolTipText("Login -  minimum 3 znaki");
		textFieldLogin.addCaretListener(new CaretListener() {
			public void caretUpdate(CaretEvent e) {
				lu.userDataValidator(textFieldLogin.getText(), lblWarningLogin);
			}
		});
		textFieldLogin.setOpaque(false);
		textFieldLogin.setForeground(Color.WHITE);
		textFieldLogin.setFont(new Font(fontName, Font.BOLD, 24));
		textFieldLogin.setColumns(10);
		textFieldLogin.setCaretColor(Color.WHITE);
		textFieldLogin.setBorder(new MatteBorder(0, 0, 2, 0, (Color) new Color(255, 255, 255)));
		textFieldLogin.setBounds(84, 427, 320, 40);
		panel_1.add(textFieldLogin);

		lblPassword = new JLabel("Has\u0142o");
		lblPassword.setForeground(Color.WHITE);
		lblPassword.setFont(new Font(fontName, Font.BOLD, 24));
		lblPassword.setBounds(84, 502, 320, 40);
		panel_1.add(lblPassword);

		passwordField = new JPasswordField();
		passwordField.setToolTipText(
				"Has\u0142o - minimum 4 znaki, jedna wielka i ma\u0142a litera, cyfra i znak specjalny");
		passwordField.addCaretListener(new CaretListener() {
			public void caretUpdate(CaretEvent e) {
				lu.userPasswordValidator(passwordField.getPassword(), lblWarningPassword);
			}
		});
		passwordField.setOpaque(false);
		passwordField.setForeground(Color.WHITE);
		passwordField.setFont(new Font(fontName, Font.BOLD, 28));
		passwordField.setCaretColor(Color.WHITE);
		passwordField.setBorder(new MatteBorder(0, 0, 2, 0, (Color) new Color(255, 255, 255)));
		passwordField.setBounds(84, 546, 320, 40);
		panel_1.add(passwordField);

		bttnCreateAccount = new JButton("Utw\u00F3rz");
		bttnCreateAccount.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				bttnCreateAccount.setFont(new Font(fontName, Font.BOLD, 26));
			}

			@Override
			public void mouseExited(MouseEvent e) {
				bttnCreateAccount.setFont(new Font(fontName, Font.BOLD, 24));
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					createNewUserAccount();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		bttnCreateAccount.setOpaque(false);
		bttnCreateAccount.setForeground(new Color(60, 179, 113));
		bttnCreateAccount.setFont(new Font(fontName, Font.BOLD, 24));
		bttnCreateAccount.setBorder(new LineBorder(new Color(255, 255, 255), 2));
		bttnCreateAccount.setBackground(new Color(34, 45, 90));
		bttnCreateAccount.setBounds(35, 634, 181, 47);
		panel_1.add(bttnCreateAccount);

		lblGoToLogin = new JLabel("Przejdz do logowania");
		lblGoToLogin.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				lblGoToLogin.setFont(new Font(fontName, Font.BOLD, 20));
			}

			@Override
			public void mouseExited(MouseEvent e) {
				lblGoToLogin.setFont(new Font(fontName, Font.BOLD, 18));
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				goToLogin();
			}
		});
		lblGoToLogin.setHorizontalAlignment(SwingConstants.CENTER);
		lblGoToLogin.setForeground(Color.WHITE);
		lblGoToLogin.setFont(new Font(fontName, Font.BOLD, 18));
		lblGoToLogin.setBounds(84, 710, 320, 40);
		panel_1.add(lblGoToLogin);

		bttnClear = new JButton("Wyczy\u015B\u0107");
		bttnClear.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				bttnClear.setFont(new Font(fontName, Font.BOLD, 26));
			}

			@Override
			public void mouseExited(MouseEvent e) {
				bttnClear.setFont(new Font(fontName, Font.BOLD, 24));
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				clearData();
			}
		});
		bttnClear.setOpaque(false);
		bttnClear.setForeground(new Color(255, 51, 51));
		bttnClear.setFont(new Font(fontName, Font.BOLD, 24));
		bttnClear.setBorder(new LineBorder(new Color(255, 255, 255), 2));
		bttnClear.setBackground(new Color(34, 45, 90));
		bttnClear.setBounds(271, 634, 181, 47);
		panel_1.add(bttnClear);

		textFieldName = new JTextField();
		textFieldName.setToolTipText("Imie - minimum 3 znaki");
		textFieldName.addCaretListener(new CaretListener() {
			public void caretUpdate(CaretEvent e) {
				lu.userDataValidator(textFieldName.getText(), lblWarningName);
			}
		});
		textFieldName.setOpaque(false);
		textFieldName.setForeground(Color.WHITE);
		textFieldName.setFont(new Font(fontName, Font.BOLD, 24));
		textFieldName.setColumns(10);
		textFieldName.setCaretColor(Color.WHITE);
		textFieldName.setBorder(new MatteBorder(0, 0, 2, 0, (Color) new Color(255, 255, 255)));
		textFieldName.setBounds(84, 73, 320, 40);
		panel_1.add(textFieldName);

		JLabel lblName = new JLabel("Imie");
		lblName.setForeground(Color.WHITE);
		lblName.setFont(new Font(fontName, Font.BOLD, 24));
		lblName.setBounds(84, 29, 320, 40);
		panel_1.add(lblName);

		textFieldLastName = new JTextField();
		textFieldLastName.setToolTipText("Nazwisko - minimum 3 znaki");
		textFieldLastName.addCaretListener(new CaretListener() {
			public void caretUpdate(CaretEvent e) {
				lu.userDataValidator(textFieldLastName.getText(), lblWarningLName);
			}
		});
		textFieldLastName.setOpaque(false);
		textFieldLastName.setForeground(Color.WHITE);
		textFieldLastName.setFont(new Font(fontName, Font.BOLD, 24));
		textFieldLastName.setColumns(10);
		textFieldLastName.setCaretColor(Color.WHITE);
		textFieldLastName.setBorder(new MatteBorder(0, 0, 2, 0, (Color) new Color(255, 255, 255)));
		textFieldLastName.setBounds(84, 195, 320, 40);
		panel_1.add(textFieldLastName);

		JLabel lblLastName = new JLabel("Nazwisko");
		lblLastName.setForeground(Color.WHITE);
		lblLastName.setFont(new Font(fontName, Font.BOLD, 24));
		lblLastName.setBounds(84, 154, 320, 40);
		panel_1.add(lblLastName);

		JLabel lblDegre = new JLabel("Stopie\u0144");
		lblDegre.setForeground(Color.WHITE);
		lblDegre.setFont(new Font(fontName, Font.BOLD, 24));
		lblDegre.setBounds(84, 270, 320, 40);
		panel_1.add(lblDegre);

		comboBoxGrade = new JComboBox();
		comboBoxGrade.setToolTipText("Wybierz uprawnienia z listy");
		comboBoxGrade.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				lu.userDataValidator(comboBoxGrade.getSelectedItem().toString(), lblWarningGrade);
			}
		});
		comboBoxGrade.setModel(new DefaultComboBoxModel(new String[] { "", "Magister", "Technik" }));
		comboBoxGrade.setVerifyInputWhenFocusTarget(false);
		comboBoxGrade.setForeground(Color.WHITE);
		comboBoxGrade.setFont(new Font(fontName, Font.BOLD, 24));
		comboBoxGrade.setBorder(new MatteBorder(0, 0, 2, 0, (Color) new Color(255, 255, 255)));
		comboBoxGrade.setBackground(myBlue);
		comboBoxGrade.setBounds(84, 321, 320, 34);
		panel_1.add(comboBoxGrade);

		lblWarningName = new JLabel("*");
		lblWarningName.setToolTipText("Pole wymagane");
		lblWarningName.setVisible(false);
		lblWarningName.setFocusCycleRoot(true);
		lblWarningName.setHorizontalAlignment(SwingConstants.CENTER);
		lblWarningName.setForeground(new Color(255, 51, 51));
		lblWarningName.setFont(new Font(fontName, Font.BOLD, 24));
		lblWarningName.setBounds(414, 73, 32, 28);
		panel_1.add(lblWarningName);

		lblWarningLName = new JLabel("*");
		lblWarningLName.setToolTipText("Pole wymagane");
		lblWarningLName.setVisible(false);
		lblWarningLName.setHorizontalAlignment(SwingConstants.CENTER);
		lblWarningLName.setForeground(new Color(255, 51, 51));
		lblWarningLName.setFont(new Font(fontName, Font.BOLD, 24));
		lblWarningLName.setBounds(414, 195, 32, 28);
		panel_1.add(lblWarningLName);

		lblWarningGrade = new JLabel("*");
		lblWarningGrade.setToolTipText("Pole wymagane");
		lblWarningGrade.setHorizontalAlignment(SwingConstants.CENTER);
		lblWarningGrade.setForeground(new Color(255, 51, 51));
		lblWarningGrade.setFont(new Font(fontName, Font.BOLD, 24));
		lblWarningGrade.setBounds(414, 321, 32, 28);
		panel_1.add(lblWarningGrade);

		lblWarningLogin = new JLabel("*");
		lblWarningLogin.setToolTipText("Pole wymagane");
		lblWarningLogin.setVisible(false);
		lblWarningLogin.setHorizontalAlignment(SwingConstants.CENTER);
		lblWarningLogin.setForeground(new Color(255, 51, 51));
		lblWarningLogin.setFont(new Font(fontName, Font.BOLD, 24));
		lblWarningLogin.setBounds(414, 427, 32, 28);
		panel_1.add(lblWarningLogin);

		lblWarningPassword = new JLabel("*");
		lblWarningPassword.setToolTipText("Pole wymagane");
		lblWarningPassword.setVisible(false);
		lblWarningPassword.setHorizontalAlignment(SwingConstants.CENTER);
		lblWarningPassword.setForeground(new Color(255, 51, 51));
		lblWarningPassword.setFont(new Font(fontName, Font.BOLD, 24));
		lblWarningPassword.setBounds(414, 546, 32, 28);
		panel_1.add(lblWarningPassword);
	}
}

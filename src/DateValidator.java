import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.swing.JLabel;

public class DateValidator {
	private LocalDate rpDate;
	private LocalDate issueDate;
	private JLabel label;
	private boolean dateIsValid = false;

	public DateValidator(Date rpDate, JLabel dateLabel) {
		this.rpDate = rpDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		this.label = dateLabel;
		checkIfDateIsValid();
	}

	public DateValidator(Date rpDate, Date issDate, JLabel dateLabel) {
		this.issueDate = rpDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		this.rpDate = issDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		this.label = dateLabel;
		if (issueDate.isAfter(getActualDate())) {
			label.setText("Recepta niewa�na");
			dateIsValid = false;
		} else {
			checkIfDateIsValid();
		}
	}

	private void checkIfDateIsValid() {
		if (rpDate.isAfter(getActualDate())) {
			label.setText("Mo�liwo� realizacji po: " + rpDate);
			dateIsValid = false;
		} else {
			if (getActualDate().minusDays(31).isBefore(rpDate)) {
				label.setText("Recepta wa�na, data realizacji dla antybiotyku: " + getActualDate().minusDays(7));
				dateIsValid = true;
			} else {
				label.setText("Recepta przeterminowana");
				dateIsValid = false;
			}
		}
	}

	private LocalDate getActualDate() {
		LocalDate ld = LocalDate.now();
		return ld;
	}

	public boolean isDateIsValid() {
		return dateIsValid;
	}
}

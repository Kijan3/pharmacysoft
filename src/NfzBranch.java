
public class NfzBranch {
	private String nfzBranch;

	public NfzBranch(String branch) {
		nfzBranch(branch);
	}

	private void nfzBranch(String branch) {
		switch (branch) {
		case "1":
			nfzBranch = "Dolno�l�ski";
			break;
		case "2":
			nfzBranch = "Kujawsko-Pomorski";
			break;
		case "3":
			nfzBranch = "Lubelski";
			break;
		case "4":
			nfzBranch = "Lubuski";
			break;
		case "5":
			nfzBranch = "��dzki";
			break;
		case "6":
			nfzBranch = "Ma�opolski";
			break;
		case "7":
			nfzBranch = "Mazowiecki";
			break;
		case "8":
			nfzBranch = "Opolski";
			break;
		case "9":
			nfzBranch = "Podkarpacki";
			break;
		case "10":
			nfzBranch = "Podlaski";
			break;
		case "11":
			nfzBranch = "Pomorski";
			break;
		case "12":
			nfzBranch = "�l�ski";
			break;
		case "13":
			nfzBranch = "�wi�tokrzyski";
			break;
		case "14":
			nfzBranch = "Warmi�sko-Mazurski";
			break;
		case "15":
			nfzBranch = "Wielkopolski";
			break;
		case "16":
			nfzBranch = "Zachodniopomorski";
			break;
		default:
			nfzBranch = "Nierozpoznany oddzia� NFZ";
			break;
		}
	}

	public String getNfzBranch() {
		return nfzBranch;
	}
}

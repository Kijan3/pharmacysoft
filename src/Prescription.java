import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.time.LocalDate;
import java.util.Date;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.border.MatteBorder;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.border.LineBorder;
import com.toedter.calendar.JDateChooser;

import Validation.MyValidator;
import Validation.NpwzValidator;
import Validation.PeselValidator;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.ImageIcon;
import javax.swing.event.CaretListener;
import javax.xml.bind.Validator;
import javax.swing.event.CaretEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Prescription extends JFrame {

	private String fontName = "Gentium Basic";
	private Color myBlue = new Color(67, 94, 137);
	private Color myGreen = new Color(60, 179, 113);
	private Color myRed = new Color(255, 51, 51);

	private JPanel contentPane;
	private JTextField txtFieldRegon;
	private JTextField txtFieldPesel;
	private JTextField txtFieldPrescriptionNumber;
	private JTextField txtFieldDoctorNpwz;
	private JComboBox comboBoxNfz;
	private JLabel lblRegonInfo;
	private JLabel lblPeselInfo;
	private JLabel lblPrescriptionNumberInfo;
	private JLabel lblDoctorNpwzInfo;
	private JDateChooser dateChooserIssueDate;
	private JLabel lblDateInfo;
	private JDateChooser dateChooserRealisationDate;
	private JButton bttnEnter;
	private JButton bttnClearData;
	private JLabel lblNfzInfo;
	private JCheckBox chckbxRealisationDate;
	private JLabel lblNewLabel;

	private boolean regonNumberValid = false;
	private boolean peselValid = false;
	private boolean rpNumberValid = false;
	private boolean npwzNumberValid = false;
	private boolean rpDateIsValid = false;

	public String regonNumber;
	public String peselNumber;
	public String nfzBranchNumber;
	public String prescriptionNumber;
	public String doctorsNumber;
	public String issueDate;
	public String realisationDate;

	// ======================
	private MyValidator myValidator;

	/**
	 * Metody
	 */

	private void showData() {
//		regon: 362717401
//		pesel: 65042509074
//		nr recepty: 0216010000005609914486
//		nr lekarza: 2409957

		System.out.println("regon: " + regonNumber);
		System.out.println("pesel: " + peselNumber);
		System.out.println("nr recepty: " + prescriptionNumber);
		System.out.println("nr lekarza: " + doctorsNumber);
		System.out.println("Oddzia� NFZ: " + nfzBranchNumber);
		System.out.println("Data wystawienia: " + issueDate);
		System.out.println("Data realizacji: " + realisationDate);
		System.out.println("======================================================");
	}

	// Poprawny REGON 361505810
	private void RegonCheck() {
		String regon = txtFieldRegon.getText();
		if (regon.length() == 9 || regon.length() == 14) {
			RegonValidator rv = new RegonValidator(regon);
			if (rv.regonIsValid) {
				txtFieldRegon.setForeground(Color.WHITE);
				lblRegonInfo.setText("Numer �wiadczeniodawcy poprawny");
				regonNumber = txtFieldRegon.getText();
				regonNumberValid = rv.regonIsValid;
			} else {
				regonInvalid();

			}
		} else {
			regonInvalid();
		}
	}

	private void regonInvalid() {
		txtFieldRegon.setForeground(Color.RED);
		lblRegonInfo.setText("Wpisz poprawny numer REGON");
		regonNumber = "";
		regonNumberValid = false;
	}

	private void checkPesel() {
		String pesel = txtFieldPesel.getText();
		if (pesel.length() > 10 && pesel.length() < 12) {
			myValidator = new PeselValidator();
			if (myValidator.isValid(pesel)) {
				lblPeselInfo.setText("Pesel poprawny, ");
				// ======================================Ustawi� wy�wietlanie danych
				// ==========================
				// + myValidator.getSex() + " wiek " + (currentYear() -
				// myValidator.getBirthYear()) + " lat.");
				txtFieldPesel.setForeground(Color.WHITE);
				peselNumber = txtFieldPesel.getText();
				peselValid = true;
			} else {
				invalidPesel();
			}
		} else {
			invalidPesel();
		}
	}

	private void invalidPesel() {
		lblPeselInfo.setText("B��dny numer pesel");
		txtFieldPesel.setForeground(Color.RED);
		peselNumber = "";
		peselValid = false;
	}

//	0216010000005609914486 poprawny numer recepty do test�w

	private void checkRpNumber() {
		String rpNumber = txtFieldPrescriptionNumber.getText();
		if (rpNumber.length() == 22) {
			RpNumberValidator rnv = new RpNumberValidator(rpNumber);
			if (rnv.checkNumber()) {
				txtFieldPrescriptionNumber.setForeground(Color.WHITE);
				lblPrescriptionNumberInfo.setText("Numer recepty poprawny, oddzia� NFZ: " + rnv.OddzialWojewodzki());
				prescriptionNumber = txtFieldPrescriptionNumber.getText();
				rpNumberValid = rnv.rpNumberIsValid;
			} else {
				invalidRpNumber();
			}
		} else {
			invalidRpNumber();
		}
	}

	private void invalidRpNumber() {
		txtFieldPrescriptionNumber.setForeground(Color.RED);
		lblPrescriptionNumberInfo.setText("Niepoprawny numer recepty");
		prescriptionNumber = "";
		rpNumberValid = false;
	}

	// Poprawny numer 2409957
	private void chceckNpwzNumber() {
		String npwz = txtFieldDoctorNpwz.getText();
		int npwzLength = 7;
		if (npwz.length() == npwzLength) {
			myValidator = new NpwzValidator();
			if (myValidator.isValid(npwz)) {
				txtFieldDoctorNpwz.setForeground(Color.WHITE);
				lblDoctorNpwzInfo.setText("Numer lekarza prawid�owy");
				doctorsNumber = txtFieldDoctorNpwz.getText();
				npwzNumberValid = true;
			} else {
				npwzInvalid();
			}
		} else {
			npwzInvalid();
		}
	}

	private void npwzInvalid() {
		txtFieldDoctorNpwz.setForeground(Color.RED);
		lblDoctorNpwzInfo.setText("Wpisz prawid�owy numer lekarza");
		doctorsNumber = "";
		npwzNumberValid = false;
	}

	private void clearData() {
		txtFieldPesel.setText(null);
		txtFieldPrescriptionNumber.setText(null);
		txtFieldDoctorNpwz.setText(null);
		txtFieldRegon.setText(null);
		chckbxRealisationDate.setSelected(false);
		dateChooserRealisationDate.setDate(null);
		dateChooserRealisationDate.setVisible(false);
		dateChooserIssueDate.setDate(null);
		lblDateInfo.setText("Wybierz date wystawienia recepty");
		comboBoxNfz.setSelectedItem("");
		lblNfzInfo.setText("Wybierz oddzia� NFZ");
	}

	private int currentYear() {
		int currentYear;
		LocalDate ld = LocalDate.now();
		currentYear = ld.getYear();
		return currentYear;
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Prescription frame = new Prescription();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Prescription() {
		setTitle("Dane recepty");
		setIconImage(Toolkit.getDefaultToolkit().getImage(Prescription.class.getResource("/Images/pillsIcon.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int width = dim.width / 2 - 340;
		int height = dim.height / 2 - 450;
		setBounds(width, height, 680, 900);
		contentPane = new JPanel();
		contentPane.setBackground(myGreen);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblRegon = new JLabel("Numer \u015Awiadczeniodawcy");
		lblRegon.setForeground(Color.WHITE);
		lblRegon.setFont(new Font(fontName, Font.BOLD, 24));
		lblRegon.setBounds(30, 28, 320, 40);
		contentPane.add(lblRegon);

		txtFieldRegon = new JTextField();
		txtFieldRegon.addCaretListener(new CaretListener() {
			public void caretUpdate(CaretEvent e) {
				RegonCheck();
			}
		});
		txtFieldRegon.setToolTipText("");
		txtFieldRegon.setOpaque(false);
		txtFieldRegon.setForeground(Color.WHITE);
		txtFieldRegon.setFont(new Font("Gentium Basic", Font.BOLD, 24));
		txtFieldRegon.setColumns(10);
		txtFieldRegon.setCaretColor(Color.WHITE);
		txtFieldRegon.setBorder(new MatteBorder(0, 0, 2, 0, (Color) new Color(255, 255, 255)));
		txtFieldRegon.setBounds(30, 79, 369, 40);
		contentPane.add(txtFieldRegon);

		JLabel lblNfz = new JLabel("Oddzia\u0142 NFZ");
		lblNfz.setForeground(Color.WHITE);
		lblNfz.setFont(new Font(fontName, Font.BOLD, 24));
		lblNfz.setBounds(466, 28, 172, 40);
		contentPane.add(lblNfz);

		comboBoxNfz = new JComboBox();
		comboBoxNfz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!comboBoxNfz.getSelectedItem().toString().equals("")) {
					NfzBranch nb = new NfzBranch(comboBoxNfz.getSelectedItem().toString());
					lblNfzInfo.setText(nb.getNfzBranch());
					nfzBranchNumber = comboBoxNfz.getSelectedItem().toString();
				}

			}
		});
		comboBoxNfz.setModel(new DefaultComboBoxModel(new String[] { "", "1", "2", "3", "4", "5", "6", "7", "8", "9",
				"10", "11", "12", "13", "14", "15", "16" }));
		comboBoxNfz.setSelectedIndex(0);
		comboBoxNfz.setVerifyInputWhenFocusTarget(false);
		comboBoxNfz.setToolTipText("");
		comboBoxNfz.setForeground(Color.WHITE);
		comboBoxNfz.setFont(new Font(fontName, Font.BOLD, 24));
		comboBoxNfz.setBorder(null);
		comboBoxNfz.setBackground(myGreen);
		comboBoxNfz.setBounds(466, 79, 132, 40);
		contentPane.add(comboBoxNfz);

		lblRegonInfo = new JLabel("Numer \u015Bwiadczeniodawcy....");
		lblRegonInfo.setFont(new Font(fontName, Font.BOLD, 16));
		lblRegonInfo.setBounds(30, 130, 369, 24);
		contentPane.add(lblRegonInfo);

		JLabel lblPesel = new JLabel("PESEL Pacjenta");
		lblPesel.setForeground(Color.WHITE);
		lblPesel.setFont(new Font(fontName, Font.BOLD, 24));
		lblPesel.setBounds(30, 165, 320, 40);
		contentPane.add(lblPesel);

		txtFieldPesel = new JTextField();
		txtFieldPesel.addCaretListener(new CaretListener() {
			public void caretUpdate(CaretEvent e) {
				checkPesel();
			}
		});
		txtFieldPesel.setToolTipText("Numer PESEL 11 cyfr");
		txtFieldPesel.setOpaque(false);
		txtFieldPesel.setForeground(Color.RED);
		txtFieldPesel.setFont(new Font(fontName, Font.BOLD, 24));
		txtFieldPesel.setColumns(10);
		txtFieldPesel.setCaretColor(Color.WHITE);
		txtFieldPesel.setBorder(new MatteBorder(0, 0, 2, 0, (Color) new Color(255, 255, 255)));
		txtFieldPesel.setBounds(30, 216, 369, 40);
		contentPane.add(txtFieldPesel);

		lblPeselInfo = new JLabel("Numer PESEL");
		lblPeselInfo.setFont(new Font(fontName, Font.BOLD, 16));
		lblPeselInfo.setBounds(30, 267, 369, 24);
		contentPane.add(lblPeselInfo);

		JLabel lblPrescriptionNumber = new JLabel("Numer recepty");
		lblPrescriptionNumber.setForeground(Color.WHITE);
		lblPrescriptionNumber.setFont(new Font(fontName, Font.BOLD, 24));
		lblPrescriptionNumber.setBounds(30, 302, 320, 40);
		contentPane.add(lblPrescriptionNumber);

		txtFieldPrescriptionNumber = new JTextField();
		txtFieldPrescriptionNumber.addCaretListener(new CaretListener() {
			public void caretUpdate(CaretEvent e) {
				checkRpNumber();
			}
		});
		txtFieldPrescriptionNumber.setToolTipText("Numer recepty 22 cyfry");
		txtFieldPrescriptionNumber.setOpaque(false);
		txtFieldPrescriptionNumber.setForeground(Color.RED);
		txtFieldPrescriptionNumber.setFont(new Font(fontName, Font.BOLD, 24));
		txtFieldPrescriptionNumber.setColumns(10);
		txtFieldPrescriptionNumber.setCaretColor(Color.WHITE);
		txtFieldPrescriptionNumber.setBorder(new MatteBorder(0, 0, 2, 0, (Color) new Color(255, 255, 255)));
		txtFieldPrescriptionNumber.setBounds(30, 353, 597, 40);
		contentPane.add(txtFieldPrescriptionNumber);

		lblPrescriptionNumberInfo = new JLabel("");
		lblPrescriptionNumberInfo.setFont(new Font(fontName, Font.BOLD, 16));
		lblPrescriptionNumberInfo.setBounds(30, 404, 597, 24);
		contentPane.add(lblPrescriptionNumberInfo);

		JLabel lblDoctorNpwz = new JLabel("Numer PWZ Lekarza");
		lblDoctorNpwz.setForeground(Color.WHITE);
		lblDoctorNpwz.setFont(new Font(fontName, Font.BOLD, 24));
		lblDoctorNpwz.setBounds(30, 439, 320, 40);
		contentPane.add(lblDoctorNpwz);

		txtFieldDoctorNpwz = new JTextField();
		txtFieldDoctorNpwz.addCaretListener(new CaretListener() {
			public void caretUpdate(CaretEvent e) {
				chceckNpwzNumber();
			}
		});
		txtFieldDoctorNpwz.setToolTipText("");
		txtFieldDoctorNpwz.setOpaque(false);
		txtFieldDoctorNpwz.setForeground(Color.WHITE);
		txtFieldDoctorNpwz.setFont(new Font(fontName, Font.BOLD, 24));
		txtFieldDoctorNpwz.setColumns(10);
		txtFieldDoctorNpwz.setCaretColor(Color.WHITE);
		txtFieldDoctorNpwz.setBorder(new MatteBorder(0, 0, 2, 0, (Color) new Color(255, 255, 255)));
		txtFieldDoctorNpwz.setBounds(30, 490, 369, 40);
		contentPane.add(txtFieldDoctorNpwz);

		lblDoctorNpwzInfo = new JLabel("PWZ ....");
		lblDoctorNpwzInfo.setFont(new Font(fontName, Font.BOLD, 16));
		lblDoctorNpwzInfo.setBounds(30, 541, 369, 24);
		contentPane.add(lblDoctorNpwzInfo);

		bttnClearData = new JButton("Wyczy\u015B\u0107");
		bttnClearData.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				bttnClearData.setFont(new Font(fontName, Font.BOLD, 26));
			}

			@Override
			public void mouseExited(MouseEvent e) {
				bttnClearData.setFont(new Font(fontName, Font.BOLD, 24));
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				clearData();
			}
		});
		bttnClearData.setOpaque(false);
		bttnClearData.setForeground(myRed);
		bttnClearData.setFont(new Font(fontName, Font.BOLD, 24));
		bttnClearData.setBorder(new LineBorder(new Color(255, 255, 255), 2));
		bttnClearData.setBackground(new Color(34, 45, 90));
		bttnClearData.setBounds(356, 787, 181, 47);
		contentPane.add(bttnClearData);

		bttnEnter = new JButton("Akceptuj");
		bttnEnter.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				bttnEnter.setFont(new Font(fontName, Font.BOLD, 26));
			}

			@Override
			public void mouseExited(MouseEvent e) {
				bttnEnter.setFont(new Font(fontName, Font.BOLD, 24));
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				if (regonNumber == null || peselNumber == null || nfzBranchNumber == null || prescriptionNumber == null
						|| doctorsNumber == null) {
					JOptionPane.showMessageDialog(null, "Sprawdz czy wszystkie dane s� uzupe�nione");
				} else {
					showData();
				}

			}
		});
		bttnEnter.setOpaque(false);
		bttnEnter.setForeground(myBlue);
		bttnEnter.setFont(new Font(fontName, Font.BOLD, 24));
		bttnEnter.setBorder(new LineBorder(new Color(255, 255, 255), 2));
		bttnEnter.setBackground(new Color(34, 45, 90));
		bttnEnter.setBounds(120, 787, 181, 47);
		contentPane.add(bttnEnter);

		JLabel lblIssueDate = new JLabel("Data wystawienia");
		lblIssueDate.setForeground(Color.WHITE);
		lblIssueDate.setFont(new Font(fontName, Font.BOLD, 24));
		lblIssueDate.setBounds(30, 593, 192, 40);
		contentPane.add(lblIssueDate);

		JLabel lblRealisationDate = new JLabel("Data realizacji");
		lblRealisationDate.setForeground(Color.WHITE);
		lblRealisationDate.setFont(new Font(fontName, Font.BOLD, 24));
		lblRealisationDate.setBounds(365, 593, 172, 40);
		contentPane.add(lblRealisationDate);

		dateChooserIssueDate = new JDateChooser();
		dateChooserIssueDate.addPropertyChangeListener(new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent e) {
				if (!(dateChooserIssueDate.getDate() == null)) {
					Date rpDate = dateChooserIssueDate.getDate();
					DateValidator dv = new DateValidator(rpDate, lblDateInfo);
					rpDateIsValid = dv.isDateIsValid();
					if (rpDateIsValid) {
						lblDateInfo.setForeground(Color.BLACK);
						issueDate = dateChooserIssueDate.getDate().toString();
					} else {
						lblDateInfo.setForeground(Color.RED);
						issueDate = dateChooserIssueDate.getDate().toString();
					}
				}
			}
		});

		dateChooserIssueDate.setFont(new Font(fontName, Font.BOLD, 24));
		dateChooserIssueDate.setForeground(Color.BLACK);
		dateChooserIssueDate.setBackground(Color.WHITE);
		dateChooserIssueDate.setBounds(30, 666, 271, 34);
		contentPane.add(dateChooserIssueDate);

		dateChooserRealisationDate = new JDateChooser();
		dateChooserRealisationDate.addPropertyChangeListener(new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent e) {
				if (!(dateChooserIssueDate.getDate() == null) && !(dateChooserRealisationDate.getDate() == null)) {
					Date rpDate = dateChooserIssueDate.getDate();
					Date realisDate = dateChooserRealisationDate.getDate();
					DateValidator dv = new DateValidator(rpDate, realisDate, lblDateInfo);
					rpDateIsValid = dv.isDateIsValid();
					if (rpDateIsValid) {
						lblDateInfo.setForeground(Color.BLACK);
						realisationDate = dateChooserRealisationDate.getDate().toString();
					} else {
						lblDateInfo.setForeground(Color.RED);
					}
				}
			}
		});
		dateChooserRealisationDate.setVisible(false);
		dateChooserRealisationDate.setFont(new Font(fontName, Font.BOLD, 24));
		dateChooserRealisationDate.setForeground(Color.WHITE);
		dateChooserRealisationDate.setBorder(null);
		dateChooserRealisationDate.setBackground(Color.GREEN);
		dateChooserRealisationDate.setBounds(356, 666, 271, 34);
		contentPane.add(dateChooserRealisationDate);

		lblNfzInfo = new JLabel("Wybierz oddzia\u0142 NFZ");
		lblNfzInfo.setForeground(Color.BLACK);
		lblNfzInfo.setFont(new Font(fontName, Font.BOLD, 16));
		lblNfzInfo.setBounds(466, 135, 188, 24);
		contentPane.add(lblNfzInfo);

		lblDateInfo = new JLabel("Wybierz date wystawienia recepty");
		lblDateInfo.setForeground(Color.BLACK);
		lblDateInfo.setFont(new Font(fontName, Font.BOLD, 16));
		lblDateInfo.setBounds(30, 724, 597, 24);
		contentPane.add(lblDateInfo);

		chckbxRealisationDate = new JCheckBox("");
		chckbxRealisationDate.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (chckbxRealisationDate.isSelected()) {
					dateChooserRealisationDate.setVisible(true);
				} else {
					dateChooserRealisationDate.setVisible(false);
					realisationDate = "";
				}
			}
		});
		chckbxRealisationDate.setSize(new Dimension(30, 30));
		chckbxRealisationDate.setPreferredSize(new Dimension(30, 30));
		chckbxRealisationDate.setMinimumSize(new Dimension(30, 30));
		chckbxRealisationDate.setMaximumSize(new Dimension(30, 30));
		chckbxRealisationDate.setBackground(myGreen);
		chckbxRealisationDate.setFont(new Font(fontName, Font.BOLD, 24));
		chckbxRealisationDate.setBounds(530, 599, 30, 34);
		contentPane.add(chckbxRealisationDate);

		lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(Prescription.class.getResource("/Images/pillsBig.png")));
		lblNewLabel.setBounds(466, 202, 156, 140);
		contentPane.add(lblNewLabel);
	}
}

package Validation;

public interface MyValidator<T extends Object> {

	boolean isValid(T objectToValidate);

}

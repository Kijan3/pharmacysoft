package Validation;

public class NpwzValidator implements MyValidator {

	public boolean npwzIsValid = false;
	private int npwzInt[] = new int[7];
	private int npwzLength = 7;

	@Override
	public boolean isValid(Object t) {
		String npwz = (String) t;

		if (npwz.length() != npwzLength) {
			npwzIsValid = false;
		} else {
			for (int i = 0; i < npwzLength; i++) {
				try {
					npwzInt[i] = Integer.parseInt(npwz.substring(i, i + 1));
				} catch (NumberFormatException e) {
					npwzIsValid = false;
					break;
				}
			}
			if (npwzInt[0] != 0 && checkSum()) {
				npwzIsValid = true;
			} else {
				npwzIsValid = false;
			}
		}
		return npwzIsValid;
	}

//	public NpwzValidator(String npwz) {
//		if (npwz.length() != npwzLength) {
//			npwzIsValid = false;
//		} else {
//			for (int i = 0; i < npwzLength; i++) {
//				try {
//					npwzInt[i] = Integer.parseInt(npwz.substring(i, i + 1));
//				} catch (NumberFormatException e) {
//					npwzIsValid = false;
//					break;
//				}
//			}
//			if (npwzInt[0] != 0 && checkSum()) {
//				npwzIsValid = true;
//			} else {
//				npwzIsValid = false;
//			}
//		}
//	}

	private boolean checkSum() {
		int sum = 1 * npwzInt[1] + 2 * npwzInt[2] + 3 * npwzInt[3] + 4 * npwzInt[4] + 5 * npwzInt[5] + 6 * npwzInt[6];
		sum %= 11;

		if (sum == npwzInt[0]) {
			return true;
		} else
			return false;
	}

}

//Numer PWZ spe�nia nast�puj�ce warunki:
//
//jest unikalny,
//nie przenosi �adnej informacji (np. dotycz�cej kolejnego numeru lekarza w rejestrze, rozr�nienia lekarz - lekarz dentysta, p�ci),
//sk�ada si� z siedmiu cyfr, 
//zawiera cyfr� kontroln�,
//nie rozpoczyna si� od cyfry 0.
//Struktura numeru PWZ:
//
//KABCDEF gdzie K - cyfra kontrolna, A do F - cyfry od 0 do 9
//
//Wyznaczenie cyfry kontrolnej:
//
//Cyfra kontrolna wyznaczana jest jako modulo 11 sumy cyfr od A do F pomno�onych przez wagi o warto�ciach od 1 do 6
//
//Np. dla cyfr od A do F, b�d�cych odpowiednio: 4, 2, 5, 7, 4, 0
//
//Suma: 4*1+2*2+5*3+7*4+4*5+0*6=71
//
//Modulo: 71 mod 11 = 5
//
//Cyfra kontrolna: 5
//
//Poprawny Numer PWZ: 5425740

package Validation;

public class PeselValidator implements MyValidator {
	public boolean peselIsValid;
	private int PeselNumber[] = new int[11];

	@Override
	public boolean isValid(Object objectToValidate) {
		String pesel = (String) objectToValidate;
		int peselLength = 11;

		if (pesel.length() != peselLength) {
			peselIsValid = false;
		} else {
			for (int i = 0; i < peselLength; i++) {
				try {
					PeselNumber[i] = Integer.parseInt(pesel.substring(i, i + 1));
				} catch (NumberFormatException e) {
					peselIsValid = false;
					break;
				}
			}
			if (checkSum() && checkMonth() && checkDay()) {
				peselIsValid = true;
			} else
				peselIsValid = false;
		}
		return peselIsValid;
	}

//	public PeselValidator(String pesel) {
//		if (pesel.length() != 11) {
//			peselIsValid = false;
//		} else {
//			for (int i = 0; i < 11; i++) {
//				try {
//					PeselNumber[i] = Integer.parseInt(pesel.substring(i, i + 1));
//				} catch (NumberFormatException e) {
//					peselIsValid = false;
//					break;
//				}
//			}
//			if (checkSum() && checkMonth() && checkDay()) {
//				peselIsValid = true;
//			} else
//				peselIsValid = false;
//		}
//	}

//	public boolean isValid() {
//		return peselIsValid;
//	}

	public int getBirthYear() {
		int year;
		int month;
		year = 10 * PeselNumber[0];
		year += PeselNumber[1];
		month = 10 * PeselNumber[2];
		month += PeselNumber[3];

		if (month > 80 && month < 93) {
			year += 1800;
		} else if (month > 0 && month < 13) {
			year += 1900;
		} else if (month > 20 && month < 33) {
			year += 2000;
		} else if (month > 40 && month < 53) {
			year += 2100;
		}
		return year;
	}

	public int getBirthMonth() {
		int month;
		month = 10 * PeselNumber[2];
		month += PeselNumber[3];
		if (month > 80 && month < 93) {
			month -= 80;
		} else if (month > 20 && month < 33) {
			month -= 20;
		} else if (month > 40 && month < 53) {
			month -= 40;
		} else if (month > 60 && month < 73) {
			month -= 60;
		}
		return month;
	}

	public int getBirthDay() {
		int day;
		day = 10 * PeselNumber[4];
		day += PeselNumber[5];
		return day;
	}

	public String getSex() {
		if (peselIsValid) {
			if (PeselNumber[9] % 2 == 1) {
				return "M�czyzna";
			} else {
				return "Kobieta";
			}
		} else {
			return "";
		}
	}

	private boolean checkSum() {
		int sum = 1 * PeselNumber[0] + 3 * PeselNumber[1] + 7 * PeselNumber[2] + 9 * PeselNumber[3] + 1 * PeselNumber[4]
				+ 3 * PeselNumber[5] + 7 * PeselNumber[6] + 9 * PeselNumber[7] + 1 * PeselNumber[8]
				+ 3 * PeselNumber[9];

		sum %= 10;
		sum = 10 - sum;
		sum %= 10;

		if (sum == PeselNumber[10]) {
			return true;
		} else {
			return false;
		}
	}

	private boolean checkMonth() {
		int month = getBirthMonth();
		if (month > 0 && month < 13) {
			return true;
		} else {
			return false;
		}
	}

	private boolean checkDay() {
		int year = getBirthYear();
		int month = getBirthMonth();
		int day = getBirthDay();

		if ((day > 0 && day < 32)
				&& (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12)) {
			return true;
		} else if ((day > 0 && day < 31) && (month == 4 || month == 6 || month == 9 || month == 11)) {
			return true;
		} else if ((day > 0 && day < 30 && leapYear(year)) || (day > 0 && day < 29 || !leapYear(year))) {
			return true;
		} else
			return false;
	}

	private boolean leapYear(int year) {
		if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0) {
			return true;
		} else {
			return false;
		}
	}

}
